const mongoose = require('mongoose');

const typeSchema = new mongoose.Schema({
  description: {
    type: String,
  }
  
  });
const typeModel = mongoose.model('Type', typeSchema);
module.exports = typeModel
