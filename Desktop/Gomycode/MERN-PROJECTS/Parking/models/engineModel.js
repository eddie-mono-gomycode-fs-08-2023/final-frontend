const mongoose = require('mongoose');
const {Schema} = mongoose

const engineSchema = new mongoose.Schema({
description: {
  type: String,
},
  photo: {
    type: String,
  },
details: {
type: String,
},
user_id:{
  type: Schema.ObjectId, ref: 'User',
},

type_id:{
  type: Schema.ObjectId, ref: 'Type',

},
});

const Engine = mongoose.model('Engine', engineSchema);
module.exports = Engine;
