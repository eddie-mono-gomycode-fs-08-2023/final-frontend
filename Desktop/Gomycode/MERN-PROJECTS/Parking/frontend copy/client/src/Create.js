import React from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { addUser } from './reducers/userReducer';
import { useNavigate } from 'react-router-dom';

const Create = () => {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [photo, setPhoto] = useState('')

const users = useSelector((state) => state.users.users)
const dispatch = useDispatch()
const Navigate = useNavigate()

const createUser = (e) => {
e.preventDefault()
dispatch(addUser({id: Date.now(), name, email, photo }))
Navigate('/home')
}


return (
    <div className='d-flex w-100 vh-100 justify-content-center align-items-center'>
    <div className='w-50 border bg-secondary text-white p-5'>
    <h1>Add A User</h1>
    <form onSubmit={createUser}>
      <div>
        <label htmlFor='name'>Name:</label>
        <input type='text' name='name' className='form-control' placeholder='Enter Name' 
        onChange={e => setName(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='email'>Email:</label>
        <input type='text' name='email' className='form-control' placeholder='Enter Email' 
        onChange={e => setEmail(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='photo'>Photo:</label>
        <input type='file' name='photo' className='form-control' placeholder='Upload Photo' 
        onChange={e => setPhoto(e.target.value)}/>
      </div><br/>
      <button className='btn btn-info'>Submit</button>
    </form>
    </div>
    </div>
  );
}

export default Create;
