import React from 'react';
import {useDispatch, useSelector,  } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteUser } from './reducers/userReducer';
import './Users.css'
const Users = () => {
  const users = useSelector((state) => state.users.users)
  console.log(users);
const dispatch = useDispatch()

const handleDelete = (id) => {
dispatch(deleteUser({id: id}))
}

  return (
    <>
    <div className='Home-nav'>
    <Link to='/home'>Home</Link>
    {/* <Link to='/parking'>Parking Space</Link> */}
    {/* <Link to='/service'>Services</Link> */}
    <Link to='/users'>Users</Link>
    <Link to='/'>Sign out</Link>
    </div>

    <div className='container'>
    <h2>Users List</h2>
    <Link to='/create' className='btn btn-success my-3'>Create +</Link>
    <table className='table'>
      <thead>
        <tr>
          <th>ID</th>
          <th>NAME</th>
          <th>EMAIL</th>
          <th>PHOTO</th>
        </tr>
      </thead>
{users.map((user, index) => {
  return(
    <tr key={index}>
  <td>{user.id}</td>
  <td>{user.name}</td>
  <td>{user.email}</td>
  <td>{user.photo}</td>
  
  <td>
<Link to={`/edit/${user.id}`} className='btn btn-sm btn-primary'>Edit</Link>
<button onClick={() => handleDelete(user.id)} className='btn btn-sm btn-danger'>Delete</button>
</td>
</tr>
  )

})}
      <tbody>

      </tbody>

    </table>

      
    </div>
        </>

  );
}

export default Users;
