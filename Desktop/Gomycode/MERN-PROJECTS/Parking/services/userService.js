const userModel = require('../models/usersModel')

const index = async (res)=>{

  try {
    const data = await userModel.find();
    return data;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const find = async (id, res)=>{

  try {
    const data = await userModel.findById(id);
    return data;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const findByName = async (name, res)=>{

  try {
    const data = await userModel.find({name: name});
    return data;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const store = async (body, res)=>{

  try {

    const userSchema = new userModel(body);
    await userSchema.save();
    return userSchema;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const update = async (id, body, res)=>{

  try {
    await userSchema.findByIdAndUpdate(id, body);
    return true;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const destroy = async (id, res)=>{

  try {
    await userSchema.findByIdAndDelete(id);
    return true
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

module.exports= {index, find, store, update, destroy}