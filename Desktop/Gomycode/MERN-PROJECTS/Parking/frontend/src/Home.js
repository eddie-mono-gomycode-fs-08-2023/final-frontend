//  https://preview.themeforest.net/item/parkivia-auto-parking-car-maintenance-wordpress-theme/full_screen_preview/22640341?_ga=2.123298507.1365495793.1705511836-1785099235.1705511836
import React, { useState } from 'react';
import Footer from './Footer';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';


const Home = () => {
  const [users, setUsers] = useState([])
  const [name, setName] = useState('')
  const [phone, setPhone] = useState('')
  const [type, setType] = useState('')
  const Navigate = useNavigate()

  const homeRequests = (e) => {
    e.preventDafault()
    axios.post('http://localhost:3300/requests', {name, phone, type})
    .then(() => {
      setUsers({name, phone, type})
    })
    setName('')
    setPhone('')
    setType('')  
  }

  return (
    <div>
    <div className='Typewriter'>
    {/* <Typewriter/> */}
    </div>

    {/* Phone mode */}
    <div className='Home0-fone'>
{/* <h3>We have the best deals for airport parking slots!</h3> */}
    </div>
    <div className='Home00'>
      
<div className='Home-book'>
  {/* <h3>Instantly book your space today. Trusted by millions</h3> */}
</div>

    <div className='SignInForm'>
    <form onSubmit={() => Navigate('/moto')}>

      {/* <select name="user">
      <option value="client">Client</option>
      <option value="Admin">Admin</option>
    </select> */}
    <br/>
    <input type='text' name='name' placeholder='Your Name' onChange={(e) => setName(e.target.value)}/>
    <br/>
        <input type='text' name='email' placeholder='Your Email' onChange={(e) => setPhone(e.target.value)}/> 
        <br/>
        <input type='submit' value='Login'/>
        {/* <button onClick={homeRequests}>Request</button> */}
      </form>
    </div>

</div>

<div className='Boxes' style={{}}>
  <div className='Box1'>
    <div className='Box1Flex' style={{}}>
<div style={{width:'30%', height: '150px', border: ''}}>
<img src='images/dollar_10170175.png'/>
</div>
<div>
<a href=''><h2>Save Money</h2></a>
  <p>It’s easy to compare parking at all<br/> major airports. Booking<br/> a reservation is quick & simple!</p>
</div>
    </div>
  </div>
  <div className='Box2'>
  <div className='Box2Flex' style={{}}>
<div style={{width:'30%', height: '150px', border: ''}}>
<img src='images/stopwatch_4388328.png' alt=''/>
</div>
<div>
<a href=''><h2>Save Time</h2></a>
  <p>It’s easy to compare parking at all<br/> major airports. Booking<br/> a reservation is quick & simple!</p>
</div>
    </div>
  </div>
  <div className='Box3'>
  <div className='Box3Flex' style={{}}>
<div style={{width:'30%', height: '150px', border: ''}}>
<img src='images/lifebuoy_429183.png' alt=''/>
</div>
<div>
  <a href=''><h2>Save Stress</h2></a>
  <p>It’s easy to compare parking at all<br/> major airports. Booking<br/> a reservation is quick & simple!</p>
</div>
    </div> 
    </div>
</div>

<div className='Home000' style={{}}>
<div className='text' style={{}}>
<section>
<h1>
  Why Choose Monopark?
</h1>
<p>
<h2>We are the official providers of Airport parking<br/>You can't park closer!</h2>
</p>
</section>
</div>


<div className='img1' style={{}}>
  <img src='https://nt.global.ssl.fastly.net/binaries/content/gallery/website/national/library/membership/car-park-ticket-machine-stourhead-wiltshire-1374324.jpg?auto=webp&width=1200&crop=16:9' alt='Pay park'/>
</div>

<div className='Boxes2' style={{}}>

<div className='Boxes21' style={{display: 'flex', width: '42%', border: ''}}>
<div className='Boxes21A' style={{width:'30%', height: '200px', border: ''}}>
<img src='images/1000_F_195947590_MaThTEt4d0gWgwDJjbNePhi4Lzx1GVgT.jpg' alt=''/>
</div>
<div>
<a href=''><h2>Save Money</h2></a>
  <p className='txt1'>It’s easy to compare parking at all<br/> major airports. Booking<br/> a reservation is quick & simple!</p>
</div>
</div>

<div className='Boxes22' style={{display: 'flex', width: '42%', border: ''}}>
<div className='img-box' style={{width:'30%', height: '200px', border: ''}}>
<img src='images/1000_F_195947590_MaThTEt4d0gWgwDJjbNePhi4Lzx1GVgT.jpg' alt=''/>
</div>
<div>
<a href=''><h2>Save Money</h2></a>
  <p className='txt1'>It’s easy to compare parking at all<br/> major airports. Booking<br/> a reservation is quick & simple!</p>
</div>
  </div>

  <div className='Boxes23' style={{display: 'flex', width: '42%', border: ''}}>
<div className='img-box' style={{width:'30%', height: '200px', border: ''}}>
<img src='images/1000_F_195947590_MaThTEt4d0gWgwDJjbNePhi4Lzx1GVgT.jpg' alt=''/>
</div>
<div>
<a href=''><h2>Save Money</h2></a>
  <p className='txt1'>It’s easy to compare parking at all<br/> major airports. Booking<br/> a reservation is quick & simple!</p>
</div>
  </div>

  <div className='Boxes24' style={{display: 'flex', width: '42%', border: ''}}>
<div className='img-box' style={{width:'30%', height: '200px', border: ''}}>
<img src='images/1000_F_195947590_MaThTEt4d0gWgwDJjbNePhi4Lzx1GVgT.jpg' alt=''/>
</div>
<div>
<a href=''><h2>Save Money</h2></a>
  <p className='txt1'>It’s easy to compare parking at all<br/> major airports. Booking<br/> a reservation is quick & simple!</p>
</div>
  </div>

</div>

<div className='ParkSpaceOption'> 
    <h5>Parking Options and Rates</h5>
    <hr className='style1'/>
    <div className='ParkSpaceOption1'> 

<div className='space1'>
<section className='space1-flex'>
<span>$</span>
<h1>30</h1>
<span>/day</span>
</section>
<p>
  <h2>Premium</h2>
</p>
<p>
  <h4>This plan includes all of the services that come with a parking space!</h4>
</p>
<button>More</button>
</div>

<div className='space1'>
<section className='space1-flex'>
<span>$</span>
<h1>18</h1>
<span>/day</span>
</section>
<p>
  <h2>Standard</h2>
</p>
<p>
  <h4>Get the unlimited time and a regular parking spot at one of the lots.</h4>
</p>
<button>More</button>
</div>

<div className='space1'>
<section className='space1-flex'>
<span>$</span>
<h1>13</h1>
<span>/day</span>
</section>
<p>
  <h2>Basic</h2>
</p>
<p>
  <h4>Get the unlimited time and a regular parking spot at one of the lots.</h4>
</p>
<button>More</button>
</div>

<div className='space1'>
<section className='space1-flex'>
<span>$</span>
<h1>6</h1>
<span>/day</span>
</section>
<p>
  <h2>Economy</h2>
</p>
<p>
  <h4>Get a spot for parking at the time of arrival. No concierge services.</h4>
</p>
<button>More</button>
</div>




    </div>
    </div>

    <div style={{marginTop: '100px'}}>
      <Footer/>
    </div>
    
    
    </div>
    </div>
  );
}

export default Home;
