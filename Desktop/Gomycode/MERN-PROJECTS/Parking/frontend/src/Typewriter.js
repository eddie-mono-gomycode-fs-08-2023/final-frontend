import React from 'react';
import {useTypewriter, Cursor} from 'react-simple-typewriter'

const Typewriter = () => {
  const [TypeEffect] = useTypewriter({
    words: [' At Monopark', 'Our Clients Are Our Priority', 'We Are Trusted By Millions',
    'Choose Monopark Today', "We've The Best Deals For Airport Parking Lots!"],
    loops: {},
    typeSpeed: 100,
    deleteSpeed: 50
  })
  return (
    <div>
      <h1>{TypeEffect}</h1>
    </div>
  );
}

export default Typewriter;
