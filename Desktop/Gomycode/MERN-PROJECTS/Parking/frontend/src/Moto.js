import React, { useEffect, useState } from 'react';
import './Motorbike.css'
import BookDrop from './dropDown/BookDrop';
import CommDrop from './dropDown/CommDrop';
import ContaDrop from './dropDown/ContaDrop';
import axios from 'axios';
import Navbar from './Navbar';

const Moto = () => {

  

return (
    <div className='Home1'>
    <Navbar/>
<div className='Motorbike'>
<div className='booking'>
{/* <h2>Reserve A Motorbike Lot</h2> */}
</div>
<div className='booking-drop'>
{/* <BookDrop/> */}
</div>
    </div>

    {/* For Mobile */}

      <div className='phone'>
    <div className='phone-form'>
      <form>
          <input type='text' name='name' placeholder='Name'/>
          <input type='phone' name='phone' placeholder='Phone Number'/>
          <input type='text' name='details' placeholder='Type Of Motorbike'/>
          <br/>
          <button className='phonbtn'>Request A Text</button>
      </form>
    </div>
    </div>

    <div className='moto-img1'>
    <img src='images/bike.png'/>
    </div>
<div className='moto-text1'>
<p>
Lorem ipsum dolor sit amet, ea pri meis accusam. Et vis accusam rationibus liberavisse, 
an vix viderer admodum. Atqui docendi omittam ei has, liber constituam id vim. Eam in dico 
doming definiebas. Cum munere impetus et. Ne nam simul oblique alterum, pri solet omnium id, 
usu an munere.
</p>
</div>

<div className='moto-grid'>
<div className='moto-flex'>
<div className='moto-Lside'>
<img src='images/bike2.webp' alt=''/>
</div>

<div className='moto-Rside'>
<div>
  <h1>We Make Airport Parking Easy</h1>
</div>
<hr className='hr-style'/>
<div className='moto-Rside-txt'>
<p>
Parking lots tend to be sources of water pollution because of their extensive impervious surfaces. 
Most existing lots have limited or no facilities to control runoff. Many areas today also require minimum 
landscaping in parking lots to provide.
</p>

<p>
Many municipalities require a minimum number of parking spaces, depending on the floor area in a store or the 
number of bedrooms in an apartment complex. In the US, each state's Department of Transportation sets the ratio.
</p>

</div>
</div> 
</div>
</div>


  <div className='Comment'>
  <section>
{/* <button className='btn1'>COMMENTS</button>
<button className='btn2'>CONTACT US</button> */}
<h1>Leave A Comment</h1>
<div className='Contact-comment'>
<div className='commBtn'>
{/* <CommDrop/> */}
</div>
<div className='contBtn'>
<ContaDrop/>
</div>
</div>
</section>
{/* <form>
  <input type='text' name='name' placeholder='Your name'/>
  <input type='text' name='email' placeholder='Your Email'/> <br/>
  <textarea rows='20' cols='100'/>
  <br/>
  <input type='submit' value='Leave a comment'/>

</form> */}

  </div>



{/* ....................................................................... */}
</div>


  );
}

export default Moto;
