var express = require('express');
var router = express.Router();
var companyController = require('../controllers/companyController')


router.get('/', companyController.index);

router.get('/:id', companyController.find);

router.post('/', companyController.store);

router.put('/:id', companyController.update);

router.delete('/:id', companyController.destroy)

module.exports = router;