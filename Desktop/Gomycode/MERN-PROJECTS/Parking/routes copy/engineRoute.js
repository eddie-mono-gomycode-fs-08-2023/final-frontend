var express = require('express');
var router = express.Router();
var engineController = require('../controllers/engineController')

/* GET home page. */
router.get('/', engineController.index);

router.get('/:id', engineController.find);

router.post('/', engineController.store);

router.put('/:id', engineController.update);

router.delete('/:id', engineController.destroy);

module.exports = router;

