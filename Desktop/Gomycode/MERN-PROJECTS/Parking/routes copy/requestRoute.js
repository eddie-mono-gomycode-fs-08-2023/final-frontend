var express = require('express');
var router = express.Router()
var requestController = require('../controllers/requestController')

router.get('/', requestController.find)

router.get('/:id', requestController.find)

router.post('/', requestController.store)

router.put('/:id', requestController.update)

router.delete('/:id', requestController.destroy)

module.exports = router;