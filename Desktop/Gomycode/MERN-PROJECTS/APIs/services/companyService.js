const companyModel = require('../models/companyModel')

const index = async ()=>{

  try {
    const data = await companyModel.find();
    return data;
  } catch (error) {
    return error
  }

}

const find = async (id)=>{

  try {
    const data = await companyModel.findById(id);
    return data;
  } catch (error) {
    return error
  }

}

const findByEmail = async (email)=>{

  try {
    const data = await companyModel.find({email: email});
    return data;
  } catch (error) {
    return error
  }

}

const store = async (body)=>{

  try {

    const companySchema = new companyModel(body);
    await companySchema.save();
    return companySchema;
  } catch (error) {
    return error
  }

}

const update = async (id, body)=>{

  try {
    await companyModel.findByIdAndUpdate(id, body);
    return true;
  } catch (error) {
    return error
  }

}

const destroy = async (id)=>{

  try {
    await companyModel.findByIdAndDelete(id);
    return true
  } catch (error) {
    return error
  }

}

module.exports= {index, find, findByEmail, store, update, destroy}

