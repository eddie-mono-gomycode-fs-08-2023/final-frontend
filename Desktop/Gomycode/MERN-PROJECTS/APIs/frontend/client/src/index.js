import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { configureStore } from '@reduxjs/toolkit'
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import userReducer from './reducers/userReducer';
import engineReducer from './reducers/engineReducer';
import companyReducer from './reducers/companyReducer';
import typeReducer from './reducers/typeReducer';
import spaceReducer from './reducers/spaceReducer';
import RequetCallReducer from './reducers/RequetCallReducer';



const store = configureStore({
  reducer: {
    users: userReducer,
    engines: engineReducer,
    companies: companyReducer,
    types: typeReducer,
    spaces: spaceReducer,
    requets: RequetCallReducer
  }
  
})


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
  <React.StrictMode>
  <Provider store={store}>
    <App />
    </Provider>
  </React.StrictMode>
  </BrowserRouter>
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
// export type RootState = ReturnType<typeof store.getState
// export type AppDispatch = typeof store.dispatch

