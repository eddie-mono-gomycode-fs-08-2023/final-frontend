import React, { useState } from 'react';
import { Link } from 'react-router-dom';

const PhoneNavDrop = () => {
  const [isOpen, setIsOpen] = useState(false)

  return (
    <div className='List-Drop1'>
        <button style={{backgroundColor: 'red', height: '30px', fontSize: '10px'}} onClick={() => setIsOpen(!isOpen)}><h2>MENUS</h2></button>
        {
    isOpen && (
      <div className='List-dropitems'>
      <div>
        <Link to='home'>Home</Link> 
        </div>
        <div>
        <a href='parking'>Parking</a> 
        </div>
        <div>
        <a href='/'>Sign Out</a> 
        </div>

      </div>
    )
  }
    </div>
  );
}

export default PhoneNavDrop;