import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  companies: [],
  company: {},
  
}

const companySlice = createSlice({
  name: 'companies',
  initialState,
  reducers: {
    addCompany: (state, action) => {
      
      state.companies.push(action.payload)
    },

    updateCompany: (state, action) => {
const {id, name, surname, telephone, photo} = action.payload
      // state.user =action.payload
const uc = state.find(company => company.id === id)
if(uc) {
  uc.name = name
  uc.surname = surname
  uc.photo = photo
  uc.telephone = telephone
}
},

    deleteCompany: (state, action) => {
      const {id} = action.payload
    const uc = state.find(company => company.id === id)
    if(uc){
    return state.filter(f => f.id !== id)
    }
    }
  }
})

export const {addCompany, updateCompany, deleteCompany} = companySlice.actions
export default companySlice.reducer
