import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  users: [],
  user: {},
  
}

const userSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    addUser: (state, action) => {
      
      state.users.push(action.payload)
    },

    updateUser: (state, action) => {
const {id, name, surname, telephone, photo} = action.payload
      // state.user =action.payload
const uu = state.find(user => user.id === id)
if(uu) {
  uu.name = name
  uu.surname = surname
  uu.photo = photo
  uu.telephone = telephone
}
},

    deleteUser: (state, action) => {
      const {id} = action.payload
    const uu = state.find(user => user.id === id)
    if(uu){
    return state.filter(f => f.id !== id)
    }
    }
  }
})

export const {addUser, updateUser, deleteUser} = userSlice.actions
export default userSlice.reducer
