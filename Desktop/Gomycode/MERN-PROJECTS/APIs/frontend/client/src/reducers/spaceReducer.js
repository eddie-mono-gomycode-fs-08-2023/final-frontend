import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  spaces: [],
  space: {},
  
}

const spaceSlice = createSlice({
  name: 'spaces',
  initialState,
  reducers: {
    addSpaces: (state, action) => {
      
      state.spaces.push(action.payload)
    },

    updateSpace: (state, action) => {
const {id, description, localization, photo} = action.payload
      // state.user =action.payload
const eu = state.find(space => space.id === id)
if(eu) {
  eu.description = description
  eu.localization = localization
  eu.photo = photo
}
},

    deleteSpace: (state, action) => {
      const {id} = action.payload
    const eu = state.find(space => space.id === id)
    if(eu){
    return state.filter(f => f.id !== id)
    }
    }
  }
})

export const {addSpaces, updateSpace, deleteSpace} = spaceSlice.actions
export default spaceSlice.reducer
