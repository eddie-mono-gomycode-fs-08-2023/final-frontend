import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  engines: [],
  engine: {},
  
}

const engineSlice = createSlice({
  name: 'engines',
  initialState,
  reducers: {
    addEngine: (state, action) => {
      
      state.engines.push(action.payload)
    },

    updateEngine: (state, action) => {
const {id, description, details, photo} = action.payload
      // state.user =action.payload
const eu = state.find(engine => engine.id === id)
if(eu) {
  eu.description = description
  eu.details = details
  eu.photo = photo
}
},

    deleteEngine: (state, action) => {
      const {id} = action.payload
    const eu = state.find(engine => engine.id === id)
    if(eu){
    return state.filter(f => f.id !== id)
    }
    }
  }
})

export const {addEngine, updateEngine, deleteEngine} = engineSlice.actions
export default engineSlice.reducer
