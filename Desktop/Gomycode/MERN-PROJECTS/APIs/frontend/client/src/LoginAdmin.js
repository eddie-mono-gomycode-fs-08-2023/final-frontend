import React from 'react';
import './Layout.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Link, useNavigate } from 'react-router-dom';

const LoginAdmin = () => {
  const Navigate = useNavigate()
  return (
    <div className='Layouts'>
    <div className='Layouts-flex'>
      
      {/* Left Side */}

    <div className='Layout-left'>
    <img src='https://ppkm.org.my/wp-content/uploads/2017/05/Evolution_Car_Parks.jpg' alt='Park'/>  
    </div>

    
    {/* Right side */}

    <div className='Layout-right'>
    <h2>ADMIN</h2>
    
    <div className='Layout-form'>
    <Form>
      <Form.Group className="mb-2" controlId="formBasicEmail">
        {/* <Form.Label>Email address</Form.Label> */}
        <Form.Control type="email" placeholder="Enter email" />
        <Form.Text className="text-muted">
          {/* We'll never share your email with anyone else. */}
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        {/* <Form.Label>Password</Form.Label> */}
        <Form.Control type="password" placeholder="Password" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicCheckbox">
        <Form.Check type="checkbox" label="Remember me" />
      </Form.Group>
      <Button onClick={() => Navigate('/users')} variant="primary" type="submit">
        Submit
      </Button>
      <Link to='/'>Login As A Client</Link>

    </Form>
    </div>

    <div className='Copyright'>
      Copyright &copy; MonoPark
      </div>

    </div>

    





</div>

</div>

  );
}

export default LoginAdmin;
