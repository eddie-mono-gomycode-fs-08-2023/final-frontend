import React, { useState } from 'react';
import { Link } from 'react-router-dom';


const ParkingDrop = () => {
  const [isOpen, setIsOpen] = useState(false)

  return (
    <div className='List-Drop1'>
        <button className='List-Drop' onClick={() => setIsOpen(!isOpen)}><h2>Parking Space</h2></button>
        {
    isOpen && (
      <div className='List-dropitems'>
      <div>
        <Link to='space'>Regular</Link> 
        </div>
        <div>
        <Link to='space'>Premium</Link> 
        </div>
        <div>
        <a href='moto'>Motorbike</a> 
        </div>

      </div>
    )
  }
    </div>
  );
}

export default ParkingDrop;
