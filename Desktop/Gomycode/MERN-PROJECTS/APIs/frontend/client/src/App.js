import './App.css';
import { Routes, Route } from 'react-router-dom';
import Navbar from './Navbar';
import Layout from './Layout';
import Home from './Home';
import Users from './Users';
import Create from './Create';
import Update from './Update';
import LoginAdmin from './LoginAdmin';
import Register from './Register';
import RecordDrop from './dropDown/RecordDrop';
import Engine from './Engine';
import EngineCreate from './EngineCreate';
import Company from './Company';
import CompanyCreate from './CompanyCreate';
import Type from './Type';
import TypeCreate from './TypeCreate';
import Space from './Space';
import SpaceCreate from './SpaceCreate';
import Parking from './Parking';
import PhoneNavDrop from './dropDown/PhoneNavDrop';
import Requet from './Requet';
import RequetCreate from './RequetCreate';



function App() {
  return (

    <div className="App">
      <Navbar/>

<Routes>
<Route index element={<Layout/>} />
<Route path='/home' element={<Home/>}/>
<Route path='/users' element={<Users/>}/>
<Route path='/create' element={<Create/>}/>
<Route path='/edit/:id' element={<Update/>}/>
<Route path='/adminLog' element={<LoginAdmin/>}/>
<Route path='/register' element={<Register/>}/>
<Route path='/record' element={<RecordDrop/>}/>
<Route path='/engine' element={<Engine/>}/>
<Route path='/engineCreate' element={<EngineCreate/>}/>
<Route path='/company' element={<Company/>}/>
<Route path='/companyCreate' element={<CompanyCreate/>}/>
<Route path='/types' element={<Type/>}/>
<Route path='/typeCreate' element={<TypeCreate/>}/>
<Route path='/space' element={<Space/>}/>
<Route path='/spaceCreate' element={<SpaceCreate/>}/>
<Route path='/parking' element={<Parking/>}/>
<Route path='/phoneNav' element={<PhoneNavDrop/>}/>
<Route path='/requets' element={<Requet/>}/>
<Route path='/requetCreate' element={<RequetCreate/>}/>





</Routes>


    </div>
  );
}

export default App;
