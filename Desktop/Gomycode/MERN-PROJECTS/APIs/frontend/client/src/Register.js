import React from 'react';
import './Layout.css';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { addUser } from './reducers/userReducer';
import {Link, useNavigate } from 'react-router-dom';



const Register = () => {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [photo, setPhoto] = useState('')

  const users = useSelector((state) => state.users.users)
console.log(users)
const dispatch = useDispatch()
const Navigate = useNavigate()

const createUser = (e) => {
  e.preventDefault()
  dispatch(addUser({id: Date.now(), name, email, photo }))
  alert(' Successful! Click "Ok" And Login')
  Navigate('/')
  }

  return (
    <div className='Layout-body'>
    <div className='Layouts'>
    <div className='Layouts-flex'>
      
      {/* Left Side */}

    <div className='Layout-left'>
    <img src='https://ppkm.org.my/wp-content/uploads/2017/05/Evolution_Car_Parks.jpg' alt='Park'/>  
    </div>

    
    {/* Right side */}

    <div className='Layout-right'>
    <h2>Sign Up</h2>
    
    <div className='Layout-form'>
    <form onSubmit={createUser}>
      <div>
        <label htmlFor='name'>Name:</label>
        <input type='text' name='name' className='form-control' placeholder='Enter Name' 
        onChange={e => setName(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='email'>Email:</label>
        <input type='text' name='email' className='form-control' placeholder='Enter Email' 
        onChange={e => setEmail(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='photo'>Photo:</label>
        <input type='file' name='photo' className='form-control' placeholder='Upload Photo' 
        onChange={e => setPhoto(e.target.value)}/>
      </div><br/>
      <button className='btn btn-info'>Submit</button>
      <h6>Already have an account?<Link to='/'>Sign in</Link></h6>

    </form>
    </div>

    <div className='Copyright'>
      Copyright &copy; MonoPark
      </div>

</div>

</div>

</div>

<div className='Layout-down'>
<h2>hello</h2>
</div>

</div>
  );
}

export default Register;
