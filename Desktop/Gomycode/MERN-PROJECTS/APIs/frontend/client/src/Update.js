import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { updateUser } from './reducers/userReducer';

const Update = () => {
  const {id} = useParams()
  const users = useSelector((state) => state.users.users)
  // const {name, email, photo} = existingUser[0]
const existingUser = users.filter(f => f.id == id)
const {name, surname, phone, photo} = existingUser[0]
const [uname, setName] = useState(name)
  const [usurname, setSurname] = useState(surname)
  const [uphoto, setPhoto] = useState(photo)
  const [uphone, setPhone] = useState(phone)
  const dispatch = useDispatch()

  const handleUpdate = (e) => {
e.preventDefault()
dispatch(updateUser({
  id: id,
  name: uname,
  surname: usurname,
  photo: uphoto,
  phone: uphone
}))
  }

  return (
    <div className='d-flex w-100 vh-100 justify-content-center align-items-center'>
    <div className='w-50 border bg-secondary text-white p-5'>
    <form onSubmit={handleUpdate}>
      <div>
        <label htmlFor='name'>Name:</label>
        <input type='text' name='name' className='form-control' placeholder='Update Name' value={uname} 
          onChange={e => setName(e.target.value)} />
      </div>
      <div>
        <label htmlFor='surname'>Surname:</label>
        <input type='text' name='surname' className='form-control' placeholder='Update Surname' value={usurname} 
          onChange={e => setSurname(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='telephone'>Telephone:</label>
        <input type='phone' name='telephone' className='form-control' placeholder='Update telephone' value={uphone} 
          onChange={e => setPhone(e.target.value)}/>
        </div>
        <div>
        <label htmlFor='photo'>photo:</label>
        <input type='file' name='photo' className='form-control' placeholder='Update Photo' value={uphoto} 
          onChange={e => setPhoto(e.target.value)}/>
        </div>
        <br/>
      <button className='btn btn-info'>Update</button>
    </form>
    </div>
    </div>
  );
}

export default Update;
