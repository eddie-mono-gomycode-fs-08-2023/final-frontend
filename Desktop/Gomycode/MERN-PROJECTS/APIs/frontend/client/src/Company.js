import React from 'react';
import {useDispatch, useSelector,  } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteCompany } from './reducers/companyReducer';
import './Users.css'
import RecordDrop from './dropDown/RecordDrop';

const Company = () => {
  const companies = useSelector((state) => state.companies.companies)
  console.log(companies);
const dispatch = useDispatch()

const handleDelete = (id) => {
dispatch(deleteCompany({id: id}))
}

  return (
    <>
    <div className='Home-nav'>
    <Link to='/home'>Home</Link>
<RecordDrop/>
    {/* <Link to='/parking'>Parking Space</Link> */}
    {/* <Link to='/service'>Services</Link> */}
    {/* <Link to='/users'>Users</Link> */}
    <Link to='/'>Sign out</Link>
    </div>

    <div className='container'>
    <h3>Branch</h3>
    <Link to='/companyCreate' className='btn btn-success my-3'> Add Record +</Link>
    <table className='table'>
      <thead>
        <tr>
          <th>ID</th>
          <th>NAME</th>
          <th>ADDRESS</th>
          <th>EMAIL</th>
          <th>TELEPHONE</th>
          <th>PHOTO</th>
          <th>ACTIONS</th>
        </tr>
      </thead>
{companies.map((company, index) => {
  return(
    <tr key={index}>
  <td>{company.id}</td>
  
  <td>{company.name}</td>

  <td>{company.address}</td>

  <td>{company.email}</td>

  <td>{company.telephone}</td>

  <td>{company.photo}</td>
  <td>
<Link to={`/edit/${company.id}`} className='btn btn-sm btn-primary'>Edit</Link>
<button onClick={() => handleDelete(company.id)} className='btn btn-sm btn-danger'>Delete</button>
</td>
</tr>
  )

})}
      <tbody>

      </tbody>

    </table>

      
    </div>
        </>

  );
}

export default Company;
