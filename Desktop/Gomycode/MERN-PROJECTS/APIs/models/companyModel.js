const mongoose = require('mongoose');

const companySchema = new mongoose.Schema({
  name:{
    type: String,
  },
  address: {
    type: String,

  },
  photo: {
    type: String,
    required: false
  },
  telephone: {
    type: Number,
  },
  email: {
    type: String,
  },
},

);

const companyModel = mongoose.model('Company', companySchema);
module.exports = companyModel; 


