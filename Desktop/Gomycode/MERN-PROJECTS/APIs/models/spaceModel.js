const mongoose = require('mongoose');
const {Schema} = mongoose

const spaceSchema = new mongoose.Schema({
description: {
  type: String,
},
photo: {
  type: String,
},
localization: {
  type: String,
},
company_id:{
  type: Schema.ObjectId, ref: 'Company'},

});



const spaceModel = mongoose.model('Space', spaceSchema);
module.exports = spaceModel;