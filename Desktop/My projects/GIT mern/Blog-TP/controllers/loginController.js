const loginService = require('../services/loginService');
const signupService = require('../services/signupService');


// const index = async(req, res) => {
//   res.send('<h1>This is Login Page</h1>')
//   try {
//     const data = await loginService.index();
//     res.status(200).json(data);
//   } catch (error) {
//     res.status(error.status).json({error: error.message})
//   }
// };

const find = async (req, res) => {
  const {email, password} = req.body
  try {
    const data = await loginService.findOne({email: email})
    res.status(200).json(data);
  } catch (error) {
    res.status(error.staus).json({error: error.message})
  }
};

const store = async (req, res) => {
  const {email, password} = req.body
  try {
    const data = await signupService.store({email, password});
  
    res.status(200).json(data)
  } catch (error) {
    res.status(error.staus).json({error: error.message})

  }
};

// const update = async (req, res) => {
//   try {
//     const data = await loginService.update(req.params.id, req.body);
//     res.status(200).json(data)
//   } catch (error) {
//     res.status(error.staus).json({error: error.message})
// }
// };

// const destroy = async (req, res) => {
//   try {
//     const data = await loginService.destroy(req.params.id);
//     res.status(200).json(data)
//   } catch (error) {
//     res.status(error.staus).json({error: error.message})

//   }
// }


module.exports = {store, find};