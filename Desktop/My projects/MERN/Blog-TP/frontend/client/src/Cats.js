import React, { useEffect, useState } from 'react';
import axios from 'axios';

const Cats = () => {
  const [cats, setCats] = useState([])
  const [name, setName] = useState('')
  const [posts, setPosts] = useState('')

  useEffect(() => {
    axios.get('http://localhost:3300/cats')
    .then((response) => {
      setCats(response.data)
    })
  }, [])


    return (
    <div>
      {
        cats.map((cat) => {
          return (
            <>
              <h1>{cat.name}</h1>
              <h3>{cat.posts}</h3>

            </>
          )
        })
      }
    </div>
  );
}

export default Cats;
