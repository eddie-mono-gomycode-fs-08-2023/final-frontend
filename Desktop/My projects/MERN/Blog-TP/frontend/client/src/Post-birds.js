import React, {useState, useEffect} from 'react';
import axios from 'axios'
import { useNavigate } from 'react-router-dom';


const PostBirds = () => {
  const [name, setName] = useState('')
  const [posts, setPosts] = useState('')

  const Navigate = useNavigate()

  const PostBlog = (e) => {
    e.preventDefault()
    axios.post('http://localhost:3300/birds', {name, posts})
    .then(() => {
    setPosts({name, posts})
    })
    setName('')
    setPosts('')
    
    }

  return (
    <div>

{/* Body container Grid */}
<div className='Grid-container' style={{backgroundColor: '', width: '100%', height: '500px'}}>
{/* Flex container */}
<div className='Flex-container' style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: '100px'}}>

<>
      <form className='Post-form' style={{display: 'flex', flexDirection: 'column', width: '100%', justifyContent: 'center', alignItems: 'center'}}>
      <label><b>Name</b></label>
        <input type='text' name='name' value={name} placeholder='enter name'   onChange={(e) => setName(e.target.value)}/><br/>
        <label><b>Type Your Post</b></label>        
        <textarea type ='text' name='posts' cols={100} rows={20} value={posts} onChange={(e) => setPosts(e.target.value)}/><br/>
        <button onClick={(PostBlog)}>Submit</button>
      </form>
    </>

{/* Right Side....List of Blogs container */}
<div style={{display: 'flex', justifyContent: 'space-evenly', width: '45%', borderLeft: '30px solid blue', borderTop: '30px solid blue', borderRadius: '25px', marginLeft: ''}}>
<div className='Space-1' style={{width: '33%', height: '400px', border: ''}}>
  <img src='https://news.stanford.edu/wp-content/uploads/2020/10/Birds_Culture-1-copy.jpg' alt='bird' style={{width: '100%', height: '220px'}}/>
  <p><h3><b>Birds Blog<br/>
Birds are really cute pets with a differ lifestyle<br/>
<button className='Pet-btn' style={{backgroundColor: 'yellow', color: 'black', borderRadius: '25px'}} onClick={() => Navigate('/birds')}>Visit Blog</button>
</b></h3></p>
</div>
<div className='Space-2' style={{width: '33%', height: '400px', border: ''}}>
<img src='https://cdn.britannica.com/79/232779-050-6B0411D7/German-Shepherd-dog-Alsatian.jpg' alt='dog' style={{width: '100%', height: '220px'}}/>
<p><h3><b>Dogs Blog<br/>
Dogs are really cute pets with a differ lifestyle<br/>
<button className='Pet-btn' style={{backgroundColor: 'yellow', color: 'black', borderRadius: '25px'}} onClick={() => Navigate('/dogs')}>Visit Blog</button>
</b></h3></p>
</div>
<div className='Space-3' style={{width: '34%', height: '400px', border: ''}}>
<img src='https://i.redd.it/isy3k3x008621.jpg' alt='cat' style={{width: '100%', height: '220px'}}/>
<p><h3><b>Cats Blog<br/>
Cats are really cute pets with a differ lifestyle<br/>
<button className='Pet-btn' style={{backgroundColor: 'yellow', color: 'black', borderRadius: '25px'}} onClick={() => Navigate('/cats')}>Visit Blog</button>
</b></h3></p>

</div>
</div>

    </div>
</div>
    </div>
  );
}

export default PostBirds;
