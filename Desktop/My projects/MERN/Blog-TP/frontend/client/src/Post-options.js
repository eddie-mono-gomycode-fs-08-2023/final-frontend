import React,{useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom';
import Postbirds from './Post-birds'

const PostOptions = () => {
  const Navigate = useNavigate()
  return (
    <div>
      {/* Body container Grid */}
<div className='Grid-container' style={{backgroundColor: '', width: '100%', height: '500px'}}>
{/* Flex container inside Grid container */}
<div className='Flex-container' style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: '100px'}}>

{/* Left Side ....Button options */}
<>
<div className='Postoptions' style={{display: "flex", flexDirection: "column", marginLeft: "5%", width: "35%", gap: "20px"}}>
<button className='News-post' style={{backgroundColor: 'blue', color: "white", height: "70px", cursor: "pointer", borderRadius: "35px"}} onClick={() => Navigate('/postbirds')}><h2>News</h2></button>
<button className='Dog-post' style={{backgroundColor: 'red', color: "white", height: "70px", cursor: "pointer", borderRadius: "35px"}} onClick={() => Navigate('/postbirds')}><h2>Dog</h2></button>
<button className='Cat-post' style={{backgroundColor: 'Yellow', color: "black", height: "70px", cursor: "pointer", borderRadius: "35px"}} onClick={() => Navigate('/postbirds')}><h2>Cat</h2></button>
<button className='Bird-post' style={{backgroundColor: 'white', color: "black", height: "70px", cursor: "pointer", borderRadius: "35px"}} onClick={() => Navigate('/postbirds')}><h2>Bird</h2></button>
  
</div>


{/* Right Side....List of Blogs container */}
<div style={{display: 'flex', justifyContent: 'space-evenly', width: '45%', borderLeft: '30px solid black', borderTop: '30px solid black', borderRadius: '25px', marginRight: '5%'}}>
<div className='Space-1' style={{width: '33%', height: '400px', border: ''}}>
<img src='https://news.stanford.edu/wp-content/uploads/2020/10/Birds_Culture-1-copy.jpg' alt='bird' style={{width: '100%', height: '220px'}}/>
<p><h3><b>Birds Blog<br/>
Birds are really cute pets with a differ lifestyle<br/>
<button className='Pet-btn' style={{backgroundColor: 'yellow', color: 'black', borderRadius: '25px'}} onClick={() => Navigate('/birds')}>Visit Blog</button>
</b></h3></p>
</div>
<div className='Space-2' style={{width: '33%', height: '400px', border: ''}}>
<img src='https://cdn.britannica.com/79/232779-050-6B0411D7/German-Shepherd-dog-Alsatian.jpg' alt='dog' style={{width: '100%', height: '220px'}}/>
<p><h3><b>Dogs Blog<br/>
Dogs are really cute pets with a differ lifestyle<br/>
<button className='Pet-btn' style={{backgroundColor: 'yellow', color: 'black', borderRadius: '25px'}} onClick={() => Navigate('/dogs')}>Visit Blog</button>
</b></h3></p>
</div>
<div className='Space-3' style={{width: '34%', height: '400px', border: ''}}>
<img src='https://i.redd.it/isy3k3x008621.jpg' alt='cat' style={{width: '100%', height: '220px'}}/>
<p><h3><b>Cats Blog<br/>
Cats are really cute pets with a differ lifestyle<br/>
<button className='Pet-btn' style={{backgroundColor: 'yellow', color: 'black', borderRadius: '25px'}} onClick={() => Navigate('/cats')}>Visit Blog</button>
</b></h3></p>

</div>
</div>
</>


</div>
</div>


</div>

  );
}

export default PostOptions;
