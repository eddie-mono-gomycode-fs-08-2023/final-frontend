const catPostModel = require('../models/catPostModel');

const index = async ()=>{

  try {
    const data = await catPostModel.find();
    return data;
  } catch (error) {
return error
  }

}

const find = async (id)=>{

  try {
    const data = await catPostModel.findById(id);
    return data;
  } catch (error) {
return error
  }

}

const store = async (body)=>{

  try {

    const catPostSchema = new catPostModel(body);
    await catPostSchema.save();
    return catPostSchema;
  } catch (error) {
return error
  }

}

const update = async (id, body)=>{

  try {
    await catPostModel.findByIdAndUpdate(id, body);
    return true;
  } catch (error) {
return error
  }

}

const destroy = async (id, res)=>{

  try {
    await catPostModel.findByIdAndDelete(id);
    return true
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

module.exports= {index, find, store, update, destroy}