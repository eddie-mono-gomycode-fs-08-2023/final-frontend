const mongoose = require('mongoose');

const birdPostSchema = new mongoose.Schema({
  name:{
    type: String,
  },

  posts: {
    type: String
  }
},
{
  timestamps: true,
}
)

const BirdPost = mongoose.model('birdPost', birdPostSchema);
module.exports = BirdPost;