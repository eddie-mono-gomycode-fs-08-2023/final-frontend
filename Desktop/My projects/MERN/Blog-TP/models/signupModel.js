var mongoose = require('mongoose');

const signupSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },

  email: {
    type: String,
    unique: true,
    required: true
  },

  age: {
    type: Number,
    required: true
  },

  password: {
    type: String,
    minLength: 6,
    required: true
  },

  photo: {
    type: String,
  }
})

const Signup = mongoose.model('Signup', signupSchema);
module.exports = Signup;