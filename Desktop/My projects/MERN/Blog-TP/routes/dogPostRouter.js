const express = require('express');
var router = express.Router();
const dogpostController = require('../controllers/dogPostController')


router.get('/', dogpostController.index);
router.post('/', dogpostController.store);
router.get('/:id', dogpostController.find)
router.post('/', dogpostController.store);
router.put('/:id', dogpostController.update);
router.delete('/:id', dogpostController.destroy);

module.exports = router;