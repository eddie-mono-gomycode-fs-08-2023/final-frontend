var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  // res.render('index', { title: 'Express'});
  res.status(220).json({title: 'My work', data: {name: 'Mono'}})
});

module.exports = router; 
