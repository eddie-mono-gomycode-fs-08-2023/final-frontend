// const express = require ('express');
const updatePostModel = require ('../models/updatePostModel');

const updatePost = async(id, body, res) => {
try {
await updatePostModel.findByIdAndUpdate(id, body)
return true
} catch (error) {
  res.status(error.status).json({error: error.message})

}

}

module.exports = {updatePost};