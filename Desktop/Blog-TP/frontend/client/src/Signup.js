import React, {useState, useEffect} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import axios from 'axios'


const Home = () => {
  const Navigate = useNavigate()
const [user, setUser] = useState([])
const [blogs, setBlogs] = useState('')
const [name, setName] = useState('')
const [email, setEmail] = useState('')
const [age, setAge] = useState('')
const [password, setPassword] = useState('')
// const [photo, setPhoto] = useState('')




const signUp = (e) => {
e.preventDefault()
axios.post('http://localhost:3300/signup', {name, email, age, password})
.then(() => {
  alert('Signed up successfully: Click "OK"')
  Navigate('/postbirds')
setUser({name, email, age, password})
})
setName('')
setEmail('')
setAge('')
setPassword('')
}

// Navigate('/')


return (

<div>
<div className='Home-menu' style={{display: 'flex', justifyContent: 'space-evenly', width: '100%', height: '70px', backgroundColor: '', borderBottom: '1px solid black', alignItems: 'center'}}>
<button onClick={() => Navigate('/dogs')} style={{border: 'none', backgroundColor: 'black', color: 'white', height: '30px', width: '6%', cursor: 'pointer'}}><b>Dogs</b></button>
<button onClick={() => Navigate('/cats')} style={{border: 'none', backgroundColor: 'black', color: 'white', height: '30px', width: '6%', cursor: 'pointer'}}>Cats</button>
<button onClick={() => Navigate('/birds')} style={{border: 'none', backgroundColor: 'black', color: 'white', height: '30px', width: '6%', cursor: 'pointer'}}>Birds</button>
<button onClick={() => Navigate('/blogs')} style={{border: 'none', backgroundColor: 'black', color: 'white', height: '30px', width: '6%', cursor: 'pointer'}}>All Blogs</button>
<button onClick={() => Navigate('/login')} style={{border: 'none', backgroundColor: 'green', color: 'white', height: '30px', width: '6%', cursor: 'pointer'}}>Sign in</button>
</div>

{/* Body container Grid */}
<div className='Grid-container' style={{backgroundColor: '', width: '100%', height: '500px'}}>
{/* Flex container inside Grid container */}
<div className='Flex-container' style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: '100px'}}>

{/* Left Side ....Signup Form */}
<>
<div className='forms' style={{marginLeft: '5%', width: '40%'}}>
<form>
<h2 style={{color: 'black', fontWeight: 'bold', marginTop: '-10%'}}>Sign up</h2>

<label style={{color: 'black'}}><b>Name</b></label>
<input style={{width: '100%', height: '40px'}} type='text' name='name' value={name} placeholder='Enter your name'
  onChange={(e) => setName(e.target.value)}
/> <br/> <br/>
<label style={{color: 'black'}}><b>Email</b></label>
<input style={{width: '100%', height: '40px'}} type='text' name='email' value={email} placeholder='Enter your Email' 
  onChange={(e) => setEmail(e.target.value)}
/> <br/> <br/>
<label style={{color: 'black'}}><b>Age</b></label>
<input style={{width: '100%', height: '40px'}} type='number' name='age' value={age} placeholder='Enter your age' 
  onChange={(e) => setAge(e.target.value)}
/> <br/> <br/>
<label style={{color: 'black'}}><b>Password</b></label>
<input style={{width: '100%', height: '40px'}} type='password' name='password' value={password} placeholder='Password' required= 'true' 
  onChange={(e) => setPassword(e.target.value)}
/> <br/> <br/>
<button className="btn" onClick={signUp} style={{width: '30%', height: '50px', backgroundColor: 'green', color: 'whitesmoke', cursor: 'pointer', padding: '0px 10px'}} ><h4><b>Sign up</b></h4></button>
<h3>Already have an account?</h3>
<button onClick={() => Navigate('/login')} style={{width: '30%', height: '50px', backgroundColor: 'green', color: 'whitesmoke', cursor: 'pointer', padding: '0px 10px'}}><h4><b>Sign in</b></h4></button>


</form>
</div>

{/* Right Side....List of Blogs container */}
<div style={{display: 'flex', justifyContent: 'space-evenly', width: '45%', borderLeft: '30px solid black', borderTop: '30px solid black', borderRadius: '25px', marginRight: '5%'}}>
<div className='Space-1' style={{width: '33%', height: '400px', border: ''}}>
<img src='https://news.stanford.edu/wp-content/uploads/2020/10/Birds_Culture-1-copy.jpg' alt='bird' style={{width: '100%', height: '220px'}}/>
<p><h3><b>Birds Blog<br/>
Birds are really cute pets with a differ lifestyle<br/>
<button className='Pet-btn' style={{backgroundColor: 'yellow', color: 'black', borderRadius: '25px'}}>Visit Blog</button>
</b></h3></p>
</div>
<div className='Space-2' style={{width: '33%', height: '400px', border: ''}}>
<img src='https://cdn.britannica.com/79/232779-050-6B0411D7/German-Shepherd-dog-Alsatian.jpg' alt='dog' style={{width: '100%', height: '220px'}}/>
<p><h3><b>Dogs Blog<br/>
Dogs are really cute pets with a differ lifestyle<br/>
<button className='Pet-btn' style={{backgroundColor: 'yellow', color: 'black', borderRadius: '25px'}}>Visit Blog</button>
</b></h3></p>
</div>
<div className='Space-3' style={{width: '34%', height: '400px', border: ''}}>
<img src='https://i.redd.it/isy3k3x008621.jpg' alt='cat' style={{width: '100%', height: '220px'}}/>
<p><h3><b>Cats Blog<br/>
Cats are really cute pets with a differ lifestyle<br/>
<button className='Pet-btn' style={{backgroundColor: 'yellow', color: 'black', borderRadius: '25px'}}>Visit Blog</button>
</b></h3></p>

</div>
</div>
</>


</div>
</div>


</div>


);
}


export default Home;

