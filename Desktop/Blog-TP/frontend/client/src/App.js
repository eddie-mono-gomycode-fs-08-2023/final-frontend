import './App.css';
import Home from './Home';
import Navba from './Navba';
import Blogs from './Blogs';
import Signup from './Signup'
import Login from './Login';
import PostBirds from './Post-birds';
import { Routes, Route} from 'react-router-dom';


function App() {
  return (
    <div className="App">
    <Navba/>
    {/* <Home/> */}

    <Routes>
        <Route index element={<Home />} />
        <Route path='blogs' element={<Blogs />} />
        <Route exact path='signup' element={<Signup />} />
        <Route path='postbirds' element={<PostBirds />} />
        <Route path='login' element={<Login />} />



      </Routes>
      
    </div>
  );
}

export default App;
