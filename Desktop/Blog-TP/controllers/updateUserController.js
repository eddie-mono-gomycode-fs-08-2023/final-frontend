const updateUserService = require('../services/updateUserService');

const updateUser = async (req, res) => {
  try {
    const data = await updateUserService.update(req.params.id, req.body);
    res.status(200).json(data)
  } catch (error) {
    res.status(error.staus).json({error: error.message})
}
};

module.exports = {updateUser};