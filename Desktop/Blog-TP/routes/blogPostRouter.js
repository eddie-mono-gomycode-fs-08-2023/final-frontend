const express = require('express');
var router = express.Router();
const blogpostController = require('../controllers/blogPostController')


router.get('/', blogpostController.index);
router.post('/', blogpostController.store);
router.get('/:id', blogpostController.find)
router.post('/', blogpostController.store);
router.put('/:id', blogpostController.update);
router.delete('/:id', blogpostController.destroy);

module.exports = router;